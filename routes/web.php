<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\StudentViewController;
use App\Http\Controllers\successcontroller;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\CustomCssFile;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::view('/sample','sample');

Route::view('/login','login');
Route::post('/login_submit',[LoginController::class,'login'])->name('login_submit');
Route::view('/success','success');
Route::view('/failed','failed');

Route::get('/student',[StudentController::class,'form_student'])->name('formstudent');
Route::post('/student_view',[StudentViewController::class,'studentview'])->name('viewstudent');

//Session
Route::get('/create',[SessionController::class,'create'])->name('create');
Route::get('/using',[SessionController::class,'using'])->name('using');
Route::get('/delete',[SessionController::class,'delete'])->name('delete');
//Login
Route::get('/login',[LoginController::class,'login'])->name('login');
Route::post('/login/submit',[LoginController::class,'login_submit'])->name('loginsubmit');
Route::get('/succes',[successcontroller::class,'success'])->name('success');
Route::get('/logout',[LoginController::class,'logout'])->name('logout');
//Database
Route::get('/employee',[EmployeeController::class,'employee'])->name('employee');
Route::get('/employee/save',[EmployeeController::class,'employee_save'])->name('employee_save');
Route::post('/employee/save/submit',[EmployeeController::class,'employee_save_submit'])->name('employee_save_submit');
Route::get('/categories',[CategoriesController::class,'categories'])->name('categories');
Route::get('/categories/save',[CategoriesController::class,'categories_save'])->name('categories_save');
Route::post('/categories/save/submit',[CategoriesController::class,'categories_save_submit'])->name('categories_save_submit');
Route::get('/products/{name}/{id}',[ProductsController::class,'products'])->name('products');
Route::get('/products/save/{name}/{id}',[ProductsController::class,'product_save'])->name('product_save');
Route::post('/products/save/submit/{name}/{id}',[ProductsController::class,'product_save_submit'])->name('product_save_submit');
