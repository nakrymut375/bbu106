<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employee</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <x-header/>
        </div>
        <div class="row">
            <div class="col-3">
              <x-menu/>
            </div>
            <div class="col-9">
              <h3 class="alert alert-success text-center" role="alert">
                Employee
              </h3>
              <table class="table table-striped">
                <thead>
                    <tr>
                      <th scope="col">Emp ID</th>
                      <th scope="col">Emp Name</th>
                      <th scope="col">Gender</th>
                      <th scope="col">Telegram</th>
                      <th scope="col">Date of Birth</th>
                      <th scope="col" colspan="3" class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($employees as $value)
                    <tr>
                      <th>{{ $value->emp_id }}</th>
                      <td>{{ $value->emp_name }}</td>
                      <td>{{ $value->gender }}</td>
                      <td>{{ $value->telegram }}</td>
                      <td>{{ $value->dob }}</td>
                      <td>
                        <a href="" class="btn btn-outline-info">View</a>
                      </td>
                      <td>
                        <a href="" class="btn btn-outline-success">Update</a>
                      </td>
                      <td>
                        <a href="" class="btn btn-outline-danger">Delete</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
              <div class="d-flex justify-content-center">
                {!! $employees->links('pagination::bootstrap-5') !!}
              </div>
              <a href="/employee/save" class="btn btn-outline-primary">Add New</a>
            </div>
        </div>
        <div class="row">
            <x-footer/>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
