<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
   <script>
    function myFunction() {
        var x = document.getElementById("id_pwd");
        if (x.type === "password") {
          x.type = "text";
        } else {
          x.type = "password";
        }
      }
    </script>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
            </div>
            <div class="col-6 alert alert-secondary p-3">
                @if(Session()->has('user'))
                    @if(Session()->get('user')== "Please, Try Again!!!")
                        <h3 class="alert alert-danger text-center" role="alert">
                            {{ Session()->get('user') }}
                            {{ Session()->forget('user') }}
                        </h3>
                    @else
                        <h3 class="alert alert-primary text-center" role="alert">
                            LOGIN[Logout]
                        </h3>
                    @endif
                @else
                    <h3 class="alert alert-primary text-center" role="alert">
                        LOGIN[First]
                    </h3>
                @endif
              <form action="{{ route('loginsubmit') }}" method="POST">
                @csrf
                <div class="mb-3">
                  <label for="id_email" class="form-label">Email address</label>
                  <input type="email" class="form-control" id="id_email" name="txt_email">
                </div>
                <div class="mb-3">
                  <label for="id_pwd" class="form-label">Password</label>
                  <input type="password" class="form-control" id="id_pwd" name="txt_pwd">
                </div>
                <div class="mb-3">
                  <input type="checkbox" onclick="myFunction()">Show Password
                </div>
                <input type="submit" class="btn btn-primary" value="Submit"/>
                <input type="reset" class="btn btn-danger" value="Reset"/>
              </form>
            </div>
              <div class="col-3">
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
