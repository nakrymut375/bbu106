<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Categories</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <x-header/>
        </div>
        <div class="row">
            <div class="col-3">
              <x-menu/>
            </div>
            <div class="col-9">
              <h3 class="alert alert-success text-center" role="alert">
                Categories
              </h3>
              <table class="table table-striped">
                <thead>
                    <tr>
                      <th scope="col">Cat ID</th>
                      <th scope="col">Logo</th>
                      <th scope="col">Cat Name</th>
                      <th scope="col">Ordered By</th>
                      <th scope="col" colspan="4" class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($categories as $items)
                    <tr>
                      <th>{{ $items->cat_id }}</th>
                      <td>
                        <img src="{{ asset('/logo/'.$items->logo) }}" style="height: 50px;width:100px;" class="rounded mx-auto d-block">
                    </td>
                      <td>{{ $items->cat_name }}</td>
                      <td>{{ $items->ordered_by }}</td>
                      <td>
                        <a href="/products/{{ str_replace(' ','-',strtolower($items->cat_name)) }}/{{ $items->cat_id }}" class="btn btn-outline-secondary">Products[{{ $items->CountPro }}]</a>
                      </td>
                      <td>
                        <a href="" class="btn btn-outline-info">View</a>
                      </td>
                      <td>
                        <a href="" class="btn btn-outline-success">Update</a>
                      </td>
                      <td>
                        <a href="" class="btn btn-outline-danger">Delete</a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
              <div class="d-flex justify-content-center">
                {!! $categories->links('pagination::bootstrap-5') !!}
              </div>
              <a href="{{ route('categories_save') }}" class="btn btn-outline-primary">Add New</a>
            </div>
        </div>
        <div class="row">
            <x-footer/>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
