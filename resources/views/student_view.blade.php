<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Student</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <x-header/>
        </div>
        <div class="row">
            <div class="col-4">
              <x-menu/>
            </div>
            <div class="col-8">
              <h3 class="alert alert-success text-center" role="alert">
                View - Student
              </h3>
              <div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <b>Student Name:</b> <i>{{ $students['stuname'] }}</i>
                    </li>
                    <li class="list-group-item">
                        <b>Gender:</b> <i>{{ $students['gender'] }}</i>
                    </li>
                    <li class="list-group-item">
                        <b>Date of Birth:</b> <i>{{ $students['dob'] }}</i>
                    </li>
                    <li class="list-group-item">
                        <b>Mid Term:</b> <i>{{ $students['mid'] }}</i>
                    </li>
                    <li class="list-group-item">
                        <b>Final:</b> <i>{{ $students['final'] }}</i>
                    </li>
                    <li class="list-group-item">
                        <b>Total Score:</b> <i>{{ $students['mid'] +  $students['final']}}</i>
                    </li>
                  </ul>
              </div>
            </div>
        </div>
        <div class="row">
            <x-footer/>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
