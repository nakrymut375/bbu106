<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Categories</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <x-header/>
        </div>
        <div class="row">
            <div class="col-3">
              <x-menu/>
            </div>
            <div class="col-9">
                @if(Session()->has('insert'))
                    <h3 class="alert alert-success text-center" role="alert">
                        {{ Session()->get('insert') }}
                        {{ Session()->forget('insert') }}
                    </h3>
                @else
                    <h3 class="alert alert-success text-center" role="alert">
                        Save Categories
                    </h3>
                @endif
              <form action="{{ route('categories_save_submit') }}" method="post">
                @csrf
                <div class="m-2">
                    <label for="id_catname" class="label-control">Cat Name</label>
                    <input type="text" required name="txt_catname" id="id_catname" class="form-control">
                </div>
                <div class="m-2">
                    <label for="id_orderedby" class="label-control">Ordered By</label>
                    <input type="number" min="0" required name="txt_orderedby" id="id_orderedby" class="form-control">
                </div>
                <div class="m-2">
                    <input type="submit" name="bnt_add" value="Save" class="btn btn-outline-primary">
                    <input type="reset" name="bnt_reset" value="Reset" class="btn btn-outline-danger">
                </div>
              </form>

            </div>
        </div>
        <div class="row">
            <x-footer/>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
