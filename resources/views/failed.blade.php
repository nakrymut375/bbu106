<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Failed</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <x-header/>
        </div>
        <div class="row">
            <div class="col-4">
              <!--<x-menu/>-->
            </div>
            <div class="col-8">
              <h3 class="alert alert-danger text-center" role="alert">
                @if(Session()->has('failed'))
                    {{ Session()->get('failed') }}
                @endif
              </h3>

            </div>
        </div>
        <div class="row">
            <x-footer/>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
