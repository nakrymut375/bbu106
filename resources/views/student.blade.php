<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Student</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <x-header/>
        </div>
        <div class="row">
            <div class="col-4">
              <x-menu/>
            </div>
            <div class="col-8">
              <h3 class="alert alert-success text-center" role="alert">
                Form - Student
              </h3>
              <form action="{{ route('viewstudent') }}" method="POST">
                @csrf
                <div class="m-1">
                    <label for="id_stuname" class="label-control">Student Name:</label>
                    <input type="text" required id="id_stuname" name="txt_stuname" class="form-control">
                </div>
                <div class="m-1">
                    <label for="id_gender" class="label-control">Gender:</label>
                    <select required id="id_gender" name="txt_gender" class="form-control">
                        <option value="">--Select Gender--</option>
                        <option>Female</option>
                        <option>Male</option>
                    </select>
                </div>
                <div class="m-1">
                    <label for="id_dob" class="label-control">Date of Birth:</label>
                    <input type="date" required id="id_dob" name="txt_dob" class="form-control">
                </div>
                <div class="m-1">
                    <label for="id_mid" class="label-control">Mid Term[0-60]:</label>
                    <input type="number" required id="id_mid" name="txt_mid" class="form-control" min="0" max="60">
                </div>
                <div class="m-1">
                    <label for="id_final" class="label-control">Final[0-40]:</label>
                    <input type="number" required id="id_final" name="txt_final" class="form-control" min="0" max="40">
                </div>
                <div class="m-1">
                    <button type="submit" class="btn btn-outline-primary">Submit</button>
                    <button type="reset" class="btn btn-outline-danger">Reset</button>
                </div>
              </form>
            </div>
        </div>
        <div class="row">
            <x-footer/>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
