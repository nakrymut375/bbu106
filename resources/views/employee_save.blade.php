<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employee</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <x-header/>
        </div>
        <div class="row">
            <div class="col-3">
              <x-menu/>
            </div>
            <div class="col-9">
                @if(Session()->has('save'))
                    <h3 class="alert alert-success text-center" role="alert">
                        {{ Session()->get('save') }} is saved
                    </h3>
                @else
                    <h3 class="alert alert-success text-center" role="alert">
                        Save Employee
                    </h3>
                @endif
              <form action="{{ route('employee_save_submit') }}" method="post">
                @csrf
                <div class="m-2">
                    <label for="id_name" class="label-control">Emp Name</label>
                    <input type="text" required name="txt_empname" id="id_name" class="form-control">
                </div>
                <div class="m-2">
                    <label for="id_gender" class="label-control">Gender</label>
                    <select name="txt_gender" id="id_gender" class="form-control" required>
                        <option value="">--Select--</option>
                        <option>Female</option>
                        <option>Male</option>
                    </select>
                </div>
                <div class="m-2">
                    <label for="id_telegram" class="label-control">Telegram</label>
                    <input type="text" required name="txt_telegram" id="id_telegram" class="form-control">
                </div>
                <div class="m-2">
                    <label for="id_dob" class="label-control">Date of Birth</label>
                    <input type="date" required name="txt_dob" id="id_dob" class="form-control">
                </div>
                <div class="m-2">
                    <input type="submit" name="bnt_add" value="Save" class="btn btn-outline-primary">
                    <input type="reset" name="bnt_reset" value="Reset" class="btn btn-outline-danger">
                </div>
              </form>

            </div>
        </div>
        <div class="row">
            <x-footer/>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
