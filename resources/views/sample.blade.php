<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Basic View</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <x-header/>
        </div>
        <div class="row">
            <div class="col-4">
              <x-menu/>
            </div>
            <div class="col-8">
              <h3 class="alert alert-success text-center" role="alert">
                Form - Product
              </h3>
              <form action="" method="post">
                @csrf
                <div>
                    <label for="id_proname" class="label-control">Product Name:</label>
                    <input type="text" name="txt_proname" id="id_proname" class="form-control" required>
                </div>
                <div>
                    <label for="id_price" class="label-control">Price:</label>
                    <input type="number" name="txt_price" id="id_price" min="0"
                    class="form-control" required>
                </div>
                <div>
                    <label for="id_qty" class="label-control">Qty:</label>
                    <input type="number" name="txt_qty" id="id_qty" min="0" class="form-control" required>
                </div>
                <div class="m-3">
                    <button type="submit" name="btn_submit" class="btn btn-outline-primary">Submit</button>
                    <button type="reset" name="btn_reset" class="btn btn-outline-danger">Reset</button>
                </div>

              </form>
            </div>
        </div>
        <div class="row">
            <x-footer/>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>
