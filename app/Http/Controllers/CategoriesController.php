<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function categories(){
        //Function count
        $categories = DB::table('Categories_View')->orderBy('ordered_by')->paginate(3);
        //CREATE VIEW Categories_View AS SELECT cat_id, cat_name, ordered_by, (SELECT COUNT(*) FROM tbl_products WHERE tbl_categories.cat_id=tbl_products.cat_id) AS CountPro FROM tbl_categories
        //$count = DB::table('tbl_products')->where('cat_id')->count();
        return view('categories',compact('categories'));
    }

    public function categories_save(){
        return view('categories_save');
    }

    public function categories_save_submit(Request $request){
        //Insert Categories
        $categories = DB::table('tbl_categories')->insert([
            'cat_name'=>$request->txt_catname,
            'ordered_by'=>$request->txt_orderedby
        ]);
        if($categories){
            //Create Session
            $request->session()->put('insert',$request->txt_catname." is saved");
        }else{
            //Create Session
            $request->session()->put('insert',$request->txt_catname." cannot save");
        }
        return view('categories_save');
    }

}
