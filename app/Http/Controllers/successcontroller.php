<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class successcontroller extends Controller
{
    public function success(Request $req){
        if($req->session()->get('user')){
            return view('success');
        }else{
            return redirect()->route('login');
        }

    }
}
