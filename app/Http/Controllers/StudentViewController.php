<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentViewController extends Controller
{
    public function studentview(Request $request){
        //Create Array
        $students = [
            'stuname'=>$request->txt_stuname,
            'gender'=>$request->txt_gender,
            'dob'=>$request->txt_dob,
            'mid'=>$request->txt_mid,
            'final'=>$request->txt_final
        ];
        //Send to View
        return view('student_view',compact('students'));
    }
}
