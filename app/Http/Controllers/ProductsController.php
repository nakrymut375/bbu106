<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{
    public function products($parcatname,$parcatid){
        $products = DB::table('tbl_products')->where('cat_id',$parcatid)->orderBy('pro_name')->get();
        return view('products',compact('products'))->with('catname',$parcatname)->with('catid',$parcatid);
    }

    public function product_save($parname, $parid){
        $categories = DB::table('tbl_categories')->orderBy('cat_name')->get();
        return view('product_save',compact('categories'))->with('catname',$parname)->with('catid',$parid);
    }

    public function product_save_submit(Request $request,$parcatname,$parcatid){
        DB::table('tbl_products')->insert([
            'cat_id'=>$request->txt_catid,
            'pro_name'=>$request->txt_proname,
            'price'=>$request->txt_price
        ]);
        $request->session()->put('insert',$request->txt_proname." is inserted");
        $categories = DB::table('tbl_categories')->orderBy('cat_name')->get();
        return view('product_save',compact('categories'))->with('catname',$parcatname)->with('catid',$parcatid);
    }

}
