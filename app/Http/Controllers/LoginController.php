<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    //Login Form
    public function login(){
        return view('login');
    }
    //Login Success
    public function login_submit(Request $req){
        $email = $req->txt_email;
        $pwd = $req->txt_pwd;
        if(($email == "admin@gmail.com") && $pwd == "123"){
            //Create Session
            $req->session()->put('user','administrator');
            //Redirect Route
            return redirect()->route('success');
        }else{
            $req->session()->put('user','Please, Try Again!!!');
            return redirect()->route('login');
        }
    }
    //Logout
    public function logout(Request $req){
        $req->session()->forget('user');
        return redirect()->route('login');
    }
}
