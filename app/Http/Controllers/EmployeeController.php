<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    //Retrieve Data
    public function employee(){
        //SQL
        //$employees = DB::select('SELECT * FROM tbl_employee ORDER BY emp_name');
        $employees = DB::table('tbl_employee')->orderBy('emp_name')->paginate(2);
        return view('employee',compact('employees'));
    }

    //Save Form
    public function employee_save(){
        return view('employee_save');
    }

    //Save Submit
    public function employee_save_submit(Request $request){
        //Save
        DB::table('tbl_employee')->insert([
            'emp_name'=>$request->txt_empname,
            'gender'=>$request->txt_gender,
            'telegram'=>$request->txt_telegram,
            'dob'=>$request->txt_dob
        ]);
        //Create Session
        $request->session()->put('save',$request->txt_empname);
        //Return View
        return view('employee_save');
    }
}
