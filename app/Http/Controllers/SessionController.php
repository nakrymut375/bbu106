<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    //Create Session
    public function create(Request $req){
        //Create Session
        $req->session()->put('admin','Administrator');
        return view('create_session');
    }
    //Using Session
    public function using(){
        return view('using_session');
    }
    //Delete Session
    public function delete(){
        return view('delete_session');
    }
}
