-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 31, 2023 at 08:43 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bmc106`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `categories_view`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `categories_view`;
CREATE TABLE IF NOT EXISTS `categories_view` (
`cat_id` int(3)
,`cat_name` varchar(40)
,`ordered_by` int(3)
,`CountPro` bigint(21)
,`logo` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_categories`
--

DROP TABLE IF EXISTS `tbl_categories`;
CREATE TABLE IF NOT EXISTS `tbl_categories` (
  `cat_id` int(3) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(40) NOT NULL,
  `ordered_by` int(3) NOT NULL,
  `logo` varchar(200) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_categories`
--

INSERT INTO `tbl_categories` (`cat_id`, `cat_name`, `ordered_by`, `logo`) VALUES
(1, 'Apple', 1, 'apple.png'),
(2, 'Gallaxy Samsung', 2, 'samsung.png'),
(3, 'Oppo', 3, 'oppo.jpg'),
(4, 'Vivo', 4, 'vivo.jpg'),
(8, 'Huawei', 5, 'huawei.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

DROP TABLE IF EXISTS `tbl_employee`;
CREATE TABLE IF NOT EXISTS `tbl_employee` (
  `emp_id` int(3) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(50) NOT NULL,
  `gender` varchar(7) NOT NULL,
  `telegram` varchar(12) NOT NULL,
  `dob` date NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`emp_id`, `emp_name`, `gender`, `telegram`, `dob`) VALUES
(1, 'Dara', 'Male', '098...', '2000-10-11'),
(2, 'Kolab', 'Female', '099...', '2000-02-09'),
(3, 'Kolab Soria', 'Female', '099009123', '2000-09-12'),
(4, 'Sophy', 'Female', '096...', '2002-02-09'),
(5, 'Nara', 'Male', '098...', '2001-08-17'),
(6, 'Kolab Soria2', 'Female', '098...', '2000-01-23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

DROP TABLE IF EXISTS `tbl_products`;
CREATE TABLE IF NOT EXISTS `tbl_products` (
  `pro_id` int(3) NOT NULL AUTO_INCREMENT,
  `cat_id` int(3) NOT NULL,
  `pro_name` varchar(45) NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`pro_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`pro_id`, `cat_id`, `pro_name`, `price`) VALUES
(1, 1, 'Iphone 11', 760),
(2, 1, 'Iphone 12', 700),
(3, 2, 'Note 22', 760),
(4, 3, 'Oppo 65', 450),
(5, 4, 'Vivo 02', 230),
(6, 1, 'Iphone 12', 299),
(7, 3, 'Oppo 99', 450),
(8, 8, 'N95', 30),
(9, 2, 'Note 13', 340);

-- --------------------------------------------------------

--
-- Structure for view `categories_view`
--
DROP TABLE IF EXISTS `categories_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `categories_view`  AS SELECT `tbl_categories`.`cat_id` AS `cat_id`, `tbl_categories`.`cat_name` AS `cat_name`, `tbl_categories`.`ordered_by` AS `ordered_by`, (select count(0) from `tbl_products` where (`tbl_categories`.`cat_id` = `tbl_products`.`cat_id`)) AS `CountPro`, `tbl_categories`.`logo` AS `logo` FROM `tbl_categories` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
